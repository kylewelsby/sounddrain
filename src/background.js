'use strict'
import './sentry'
import path from 'path'
import { app, protocol, ipcMain, shell, Menu, BrowserWindow } from 'electron'
import defaultMenu from 'electron-default-menu'

import debug from 'debug'
import SoundCloud from './soundcloud'
import storage from 'electron-json-storage'
import {
  createProtocol,
  installVueDevtools
} from 'vue-cli-plugin-electron-builder/lib'

const log = debug('sounddrain:background')

const isDevelopment = process.env.NODE_ENV !== 'production'

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

const sc = new SoundCloud()
storage.get('tracks', (err, data) => {
  if (err) throw err
  if (Array.isArray(data)) {
    sc.tracks = data
  }
})
storage.get('settings', (err, data) => {
  if (err) throw err
  sc.numberOfLikesToCheck = data.numberOfLikesToCheck || 100
})

// Standard scheme must be registered before the app is ready
protocol.registerStandardSchemes(['app'], { secure: true })
function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({ width: 1204, height: 768 })

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    win.loadURL('app://./index.html')
  }

  win.on('closed', () => {
    win = null
  })
}

function autoDownloadLikesIfEnabled () {
  storage.get('settings', (err, data) => {
    if (err) throw err
    if (data.autoDownloadLikes && data.profileName) {
      sc.fetch(`https://soundcloud.com/${data.profileName}/likes`, false)
    }
  })
}

setInterval(() => {
  autoDownloadLikesIfEnabled()
}, 60 * 1000)
autoDownloadLikesIfEnabled()

const passThruKeys = ['started', 'skipped', 'error', 'processing', 'failedUrl', 'skippedUrl', 'completedUrl']

for (let key of passThruKeys) {
  sc.on(key, (payload) => {
    log(`event: ${key}`)
    win.webContents.send(key, payload)
  })
}

sc.on('completed', track => {
  log('event: completed')
  storage.get('tracks', (err, data) => {
    if (err) throw err
    if (!Array.isArray(data)) {
      data = []
    }
    data.push(track)
    storage.set('tracks', data, (err, data) => {
      if (err) throw err
      win.webContents.send('db:saved')
    })
  })
  win.webContents.send('completed', track)
})

ipcMain.on('getInitialState', (event, url) => {
  storage.getAll(function (error, data) {
    if (error) throw error
    win.webContents.send('initialState', data)
  })
})

ipcMain.on('fetch', (event, url) => {
  sc.fetch(url)
})

ipcMain.on('redownload', (event, url) => {
  storage.get('tracks', (err, data) => {
    if (err) throw err
    const tracks = data.filter(t => t.url !== url)
    storage.set('tracks', tracks, (err, data) => {
      if (err) throw err
      log('redownloading', url)
      sc.tracks = tracks
      sc.fetch(url)
    })
  })
})

ipcMain.on('clearHistory', (event) => {
  log('clearing history')
  storage.set('tracks', [], (err, data) => {
    if (err) throw err
    win.webContents.send('db:saved')
    storage.getAll(function (error, data) {
      if (error) throw error
      win.webContents.send('initialState', data)
    })
  })
})

ipcMain.on('saveSettings', (event, settings) => {
  storage.set('settings', settings, (err, data) => {
    if (err) throw err
    autoDownloadLikesIfEnabled()
    win.webContents.send('db:saved')
  })
})

ipcMain.on('openExternal', function (event, url) {
  shell.openExternal(url)
})

ipcMain.on('showInFinder', function (event,file) {
  if (!file) {
    file = path.resolve(app.getPath('music'), 'SoundDrain')
  }
  shell.showItemInFolder(file)
})

const menu = defaultMenu(app, shell)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  Menu.setApplicationMenu(Menu.buildFromTemplate(menu))

  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      await installVueDevtools()
    } catch (e) {
      console.error('Vue Devtools failed to install:', e.toString())
    }
  }
  createWindow()
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', data => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}
