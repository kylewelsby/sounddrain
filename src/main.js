import './plugins/sentry'
import Vue from 'vue'
import './plugins/fontawesome'
import './plugins/vuetify'
import './plugins/analytics'
import './plugins/coffee'
import App from './App.vue'
import router from './router'
import 'roboto-fontface/css/roboto/roboto-fontface.css'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
