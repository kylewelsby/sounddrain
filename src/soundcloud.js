import './sentry'
import EventEmitter from 'events'
import path from 'path'
import fs from 'fs'
import debug from 'debug'
import NodeID3 from 'node-id3'
import axios from 'axios'
import sanitize from 'sanitize-filename'
import { get } from 'shvl'
import { app, session, BrowserWindow } from 'electron'

const log = debug('sounddrain:soundcloud')
const USER_AGENT = 'Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3'

export default class SoundCloud extends EventEmitter {
  constructor () {
    super()
    this.clientID = null
    this.window = null
    this.tracks = []
    this.numberOfLikesToCheck = 100
  }

  async fetch (url, emitEvents = true) {
    const startedAt = new Date()
    if (emitEvents) {
      this.emit('started', {
        url: url
      })
    }
    const tracks = await this.convertURLtoTracks(url)
    log('Fetch Tracks', tracks.length)

    let processed = 0
    let progress = 0
    for (let track of tracks) {
      progress++
      if (emitEvents && tracks.length > 1) {
        this.emit('processing', {
          url: url,
          isSet: true,
          startedAt: startedAt,
          progress: progress / tracks.length
        })
      }
      if (this.hasTrack(track)) {
        if (emitEvents) {
          this.emit('skipped', track)
          log('Skipping Track', track.id)
        }
      } else {
        processed++
        track.startedAt = new Date()
        this.emit('processing', track)
        this.tracks.push(track)
        await this.fetchTrack(track)
        track.completedAt = new Date()
        this.emit('completed', track)
        log('Finished Track', track.id)
      }
    }
    if (emitEvents && tracks > 0 && processed === 0) {
      this.emit('failedUrl', url)
    }

    if (emitEvents && tracks === 0 && processed === 0) {
      this.emit('skippedUrl', url)
    }

    if (emitEvents) {
      this.emit('completedUrl', url)
    }

    log('Completed')
  }

  hasTrack (track) {
    return this.tracks.some(downloadedTrack => {
      return downloadedTrack.id === track.id
    })
  }

  get tmpDir () {
    return app.getPath('temp')
  }

  get musicDir () {
    return path.resolve(app.getPath('music'), 'SoundDrain')
  }

  async convertURLtoTracks (url) {
    const promise = new Promise((resolve, reject) => {
      log('Loading', url)

      const timeout = setTimeout(() => {
        this.emit('error', {
          url: url,
          errorReason: 'Could not find track(s)',
          errorAt: new Date()
        })
        reject(new Error('Timeout: could not find tracks within 10 seconds'))
      }, 10 * 1000)

      const urls = {
        urls: [
          'https://api-mobi.soundcloud.com/resolve*',
          'https://api-mobi.soundcloud.com/users/*/profile*'
        ]
      }
      session.defaultSession.webRequest.onResponseStarted(urls, ({ url }) => {
        const parsedUrl = new URL(url)
        this.clientID = parsedUrl.searchParams.get('client_id')
        log('Found', url)
        axios({
          url: url
        }).then(async (response) => {
          const data = response.data
          let tracks = []
          if (data.likes) {
            log('is profile with likes')
            clearTimeout(timeout)
            tracks = await this.profileLikes(tracks, data.likes)
          } else if (['track', 'playlist'].includes(data.kind)) {
            log('is track or playlist')
            if (data.kind === 'playlist') {
              clearTimeout(timeout)
            }
            tracks = await this.processTrackData(data)
          } else {
            log('data')
          }
          log('Tracks', tracks.length)
          if (tracks.length > 0) {
            clearTimeout(timeout)
            resolve(tracks)
          }
        })
      })

      this.window = new BrowserWindow({ show: false })
      this.window.webContents.clearHistory()
      this.window.webContents.setAudioMuted(true)
      this.window.loadURL(url, { userAgent: USER_AGENT })
    })
    return promise
      .then(tracks => tracks.filter(Boolean))
      .then(tracks => {
        log('Tracks Found', tracks.length)
        this.window.close()
        this.window = null

        return tracks
      })
  }

  async profileLikes (tracks, data) {
    log('is profile with likes')
    const likedTracks = data.collection.map(collection => collection.track)
    const playlist = {
      kind: 'playlist',
      tracks: likedTracks
    }

    const playlistTracks = await this.processTrackData(playlist)
    playlistTracks.forEach(track => {
      tracks.push(track)
    })

    if (data.next_href && tracks.length < this.numberOfLikesToCheck) {
      const response = await axios({
        url: data.next_href + `&client_id=${this.clientID}`
      })
      await this.profileLikes(tracks, response.data)
    }
    return tracks
  }

  async processTrackData (data) {
    if (data.kind === 'playlist' && data.tracks) {
      const list = data.tracks.filter(Boolean).filter(track => track.kind === 'track')
      const array = []
      for (let item of list) {
        const trackData = await this.formattedTrackData(item)
        array.push(trackData)
      }
      return array
    } else if (data.kind === 'track') {
      return [this.formattedTrackData(data)]
    }
    return []
  }

  async formattedTrackData (track) {
    if(!track.media) {
      log('Track', track)
      const response = await axios({
        url: `https://api-mobi.soundcloud.com/tracks/${track.id}`,
        params: {
          client_id: this.clientID,
          format: 'json'
        }
      })
      track = response.data
      log('track response', track)
    }
    const transcoding = track.media.transcodings.find(t => {
      return t.format.protocol === 'progressive' && t.format.mime_type === 'audio/mpeg'
    })
    return {
      id: track.id,
      url: track.permalink_url,
      artwork: track.artwork_url,
      title: track.title,
      artist: get(track, 'publisher_metadata.artist') || track.user.username,
      composer: get(track, 'publisher_metadata.writer_composer'),
      username: track.user.username,
      date: track.display_date.split('T')[0],
      year: track.display_date.split('-')[0],
      audio: (transcoding || {}).url
    }
  }

  async fetchTrack (track) {
    log('Fetch Track', track)
    const basename = sanitize(`${track.artist} - ${track.title}`)
    const filenames = {
      audio: path.resolve(this.musicDir, `${basename}.mp3`),
      artwork: path.resolve(this.tmpDir, `${basename}.jpeg`)
    }
    track.filename = filenames.audio
    const audioResponse = axios({
      url: track.audio,
      params: {
        client_id: this.clientID
      }
    }).then(response => {
      return this.fetchData(response.data.url, filenames.audio, (progressEvent) => {
        track.progress = progressEvent.loaded
        log(`Download progress: ${progressEvent.loaded}`)
        this.emit('processing', track)
      })
    })
    let artworkResponse = Promise.resolve()
    if (track.artwork) {
      artworkResponse = this.fetchData(track.artwork, filenames.artwork)
    }
    return Promise.all([
      audioResponse,
      artworkResponse
    ]).then(() => {
      track.progress = 0.99
      this.emit('processing', track)
      return this.writeID3Tag(track, filenames)
    }).then(() => {
      track.progress = 1
      this.emit('processing', track)
    })
      .catch(error => {
        log('error', error)
        track.errorAt = new Date()
        track.errorReason = error.message
        this.emit('error', track)
      })
  }

  async writeID3Tag (track, filenames) {
    log('Write ID3 Tag', filenames.audio)
    const meta = {
      title: track.title,
      artist: track.artist,
      fileOwner: track.url,
      year: track.year,
      date: track.date,
      composer: track.composer,
      image: filenames.artwork
    }
    let success = NodeID3.write(meta, filenames.audio)
    if (!success) {
      return Promise.reject(new Error('Could not write ID3 tag'))
    } else {
      return Promise.resolve(track)
    }
  }

  async fetchData (url, filename, onDownloadProgress = null) {
    log('Fetch Data', url, filename, this.clientID)
    const response = await axios({
      url: url,
      responseType: 'stream'
    }).then(response => {
      const writer = fs.createWriteStream(filename)
      response.data.pipe(writer)
      if (onDownloadProgress) {
        const total = response.headers['content-length']
        let downloaded = 0
        let loaded = 0
        const throttle = setInterval(() => {
          onDownloadProgress({
            loaded: parseFloat(loaded.toFixed(2))
          })
        }, 1000)
        response.data.on('data', (data) => {
          loaded = (downloaded += Buffer.byteLength(data) / total)
        })
        response.data.on('end', () => clearInterval(throttle))
      }
      return new Promise((resolve, reject) => {
        writer.on('finish', resolve)
        writer.on('error', reject)
      })
    })
    log('Finished')
    return response
  }
}
