import Vue from 'vue'
import Vuetify, {
  VApp,
  VList,
  VListTile,
  VSpacer,
  VLayout
} from 'vuetify/lib'
import { Ripple } from 'vuetify/lib/directives'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// import 'vuetify/src/stylus/app.styl'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify, {
  iconfont: 'faSvg',
  icons: {
    check: {
      component: FontAwesomeIcon,
      props: {
        icon: ['far', 'check']
      }
    },
    search: {
      component: FontAwesomeIcon,
      props: {
        icon: ['far', 'search']
      }
    },
    settings: {
      component: FontAwesomeIcon,
      props: {
        icon: ['far', 'ellipsis-v']
      }
    },
    medal: {
      component: FontAwesomeIcon,
      props: {
        icon: ['fas', 'medal']
      }
    }
  },
  components: {
    VApp,
    VList,
    VListTile,
    VSpacer,
    VLayout
  },
  directives: {
    Ripple
  },
  theme: {
    primary: '#ff8800',
    secondary: '#424242',
    accent: '#ff8800',
    error: '#f44336',
    warning: '#ffeb3b',
    info: '#2196f3',
    success: '#4caf50'
  }
})
