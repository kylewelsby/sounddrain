import Vue from 'vue'
import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'

Sentry.init({
  dsn: 'https://6818943081154f17afb943ee58afc85f@sentry.io/1462794',
  integrations: [
    new Integrations.Vue({
      Vue,
      attachProps: true
    })
  ]
})
