import Vue from 'vue'
import { ipcRenderer } from 'electron'

Vue.mixin({
  methods: {
    coffee () {
      let url = 'https://ko-fi.com/A3403WZD'
      this.$ga.social('Ko-Fi', 'buy', url)
      ipcRenderer.send('openExternal', url)
    }
  }
})
