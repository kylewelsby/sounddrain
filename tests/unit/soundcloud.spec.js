import fs from 'fs'
import path from 'path'
import nock from 'nock'
// import { app } from 'electron'
import SoundCloud from '@/soundcloud'

beforeEach(() => {
  nock.disableNetConnect()
})

test('downloads file', async () => {
  nock('https://api-mobi.soundcloud.com')
    .get('/resolve')
    .query(true)
    .replyWithFile(200, path.resolve(__dirname, '../../__fixtures__/resolve-track.json'), {
      'Content-Type': 'application/json'
    })
    .get('/media/soundcloud:tracks:545720088/2345df1e-3fee-453d-a634-15bc0a9faef3/stream/progressive')
    .query(true)
    .replyWithFile(200, path.resolve(__dirname, '../../__fixtures__/progressive-media.json'), {
      'Content-Type': 'application/json'
    })
  nock('https://i1.sndcdn.com')
    .get('/artworks-000459247398-1nlgn1-original.jpg')
    .replyWithFile(200, path.resolve(__dirname, '../../__fixtures__/artwork.jpeg'))
  nock('https://cf-media.sndcdn.com')
    .get('/lHLrWrQnBTRc.128.mp3')
    .query(true)
    .replyWithFile(200, path.resolve(__dirname, '../../__fixtures__/audio.mpga'))

  const s = new SoundCloud()
  await s.fetch('https://soundcloud.com/pearlywhi7es/lemzly-dale-acme')
  const file = path.resolve(__dirname, '../tmp/Music/SoundDrain/Lemzly Dale - Lemzly Dale - ACME.mp3')
  expect(fs.existsSync(file)).toBeTruthy()
})
