// jest.mock('electron')
// import { shallowMount } from '@vue/test-utils'
// import Component from '@/components/HelloWorld.vue'
// import electron from 'electron'
//
// describe('HelloWorld.vue', () => {
//   it('renders props.msg when passed', () => {
//     let processingCallback = null
//     let completedCallback = null
//     electron.ipcRenderer.on = jest.fn().mockImplementation((string, callback) => {
//       if (string === 'processing') {
//         processingCallback = callback
//       }
//       if (string === 'completed') {
//         completedCallback = callback
//       }
//     })
//     const wrapper = shallowMount(Component)
//     wrapper.vm._data.tracks.push({
//       url: 'https://soundcloud.com/mekyle/is-it-easy'
//     })
//     processingCallback(null, {
//       url: 'https://soundcloud.com/mekyle/is-it-easy',
//       startedAt: '2019-04-01T00:00:00Z'
//     })
//     completedCallback(null, {
//       url: 'https://soundcloud.com/mekyle/is-it-easy',
//       completedAt: '2019-04-01T00:00:00Z'
//     })
//     expect(wrapper.vm._data.tracks).toContainEqual({
//       url: 'https://soundcloud.com/mekyle/is-it-easy',
//       startedAt: '2019-04-01T00:00:00Z',
//       completedAt: '2019-04-01T00:00:00Z'
//     })
//   })
// })
