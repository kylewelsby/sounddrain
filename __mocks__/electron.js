import path from 'path'
class BrowserWindow {
  constructor () {}
  get webContents () {
    return {
      clearHistory: jest.fn(),
      setAudioMuted: jest.fn()
    }
  }
  loadURL (url, options) {}
  close () {}
}
module.exports = {
  app: {
    getPath (key) {
      if (key === 'music') {
        return 'tests/tmp/Music'
      }
      return 'tests/tmp'
    }
  },
  ipcRenderer: {
    on: jest.fn()
  },
  BrowserWindow: BrowserWindow,
  session: {
    defaultSession: {
      webRequest: {
        onResponseStarted (filter, fn) {
          fn({
            url: 'https://api-mobi.soundcloud.com/resolve?permalink_url=https://soundcloud.com/tayiwar/01-satisfied?in=soulection/sets/tay-iwar-gemini&client_id=iZIs9mchVcX5lhVRyQGGAYlNPVldzAoX'
          })
        }
      }
    }
  }
}
